<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('/menu', function () {
    return view('menu');
});

Route::get('/menu/puding/{jenis?}',function ($jenis='panacotta') {
    return view('puding',['jenis'=>$jenis]);
});

/*Route::get('/helloworld', function () {
    return 'Hello World, Pak Dosen';
});*/
/*Route::get('/menu/{cat_id}/{id?}', function($cat_id,$id = "ALL") {
    return "MENU = ". $cat_id.  "MENU = ". $id;
})->name('catid'); */

/*Route::get('greeting/(nama)',function($nama) {
    return view('namafilebaru',['namapeserta' =>$nama]);
});*/






Route::view('/selamatdatang','welcome');